Source: uwsgi
Section: httpd
Priority: optional
Maintainer: uWSGI packaging team <pkg-uwsgi-devel@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
 Alexandre Rossi <niol@zincube.net>,
Build-Depends:
 debhelper-compat (= 13),
 python3,
 help2man <!nodoc>,
 libacl1-dev,
 libcap-dev [linux-any],
 libcurl4-openssl-dev,
 libgeoip-dev,
 libgloox-dev,
 libjail-dev [kfreebsd-any],
 libjansson-dev,
 libkvm-dev [kfreebsd-any],
 libldap2-dev,
 libpam0g-dev,
 libpcre2-dev,
 libpq-dev,
 libsqlite3-dev,
 libssl-dev,
 libsystemd-dev [!kfreebsd-any !hurd-any] | libsystemd-journal-dev [!kfreebsd-any !hurd-any],
 libwrap0-dev,
 libxml2-dev,
 libxslt1-dev,
 libyajl-dev,
 libyaml-dev,
 libzmq5-dev | libzmq3-dev [!hurd-any] | libzmq-dev [!kfreebsd-any !hurd-any],
 shellcheck <!nocheck>,
 uuid-dev,
 zlib1g-dev,
Standards-Version: 4.7.2
Homepage: http://projects.unbit.it/uwsgi/
Vcs-Git: https://salsa.debian.org/uwsgi-team/uwsgi.git
Vcs-Browser: https://salsa.debian.org/uwsgi-team/uwsgi
Rules-Requires-Root: no

Package: uwsgi
Architecture: any
Depends:
 uwsgi-core (= ${binary:Version}),
 ${misc:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Description: fast, self-healing application container server
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It uses the uwsgi protocol for all the networking/interprocess communications.
 .
 uWSGI can be run in preforking, threaded, asynchronous/evented modes and
 supports various forms of green threads/coroutines (such as uGreen, Greenlet,
 Fiber). uWSGI provides several methods of configuration: via command line,
 via environment variables, via XML, INI, YAML configuration files, via LDAP
 and more.
 .
 On top of all this, it is designed to be fully modular. This means that
 different plugins can be used in order to add compatibility with tons of
 different technology on top of the same core.
 .
 This package depends on uWSGI core binary and installs:
  * init.d script for running uWSGI daemon(s) with options defined in custom
    user-created configuration files
  * infrastructure for running daemons (like common locations of communication
    sockets, logs)

Package: uwsgi-core
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 nginx-full | libapache2-mod-proxy-uwsgi,
 uwsgi-dev,
 uwsgi-extra,
Provides:
 ${uwsgi:Provides},
Description: fast, self-healing application container server (core)
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It uses the uwsgi protocol for all the networking/interprocess communications.
 .
 uWSGI can be run in preforking, threaded, asynchronous/evented modes and
 supports various forms of green threads/coroutines (such as uGreen, Greenlet,
 Fiber). uWSGI provides several methods of configuration: via command line,
 via environment variables, via XML, INI, YAML configuration files, via LDAP
 and more.
 .
 On top of all this, it is designed to be fully modular. This means that
 different plugins can be used in order to add compatibility with tons of
 different technology on top of the same core.
 .
 This package provides core uWSGI binary (built without any embedded
 plugins) and plugins with no or few dependencies:
 .
 ${uwsgi:corepluginlist}
 .
 forkptyrouter, systemd and tuntap are provided
 only on supported architectures.
 .
 Developing external uWSGI plugin requires the package uwsgi-dev.

Package: uwsgi-dev
Architecture: any
Multi-Arch: foreign
Section: libdevel
Depends:
 uwsgi-src (= ${source:Version}),
 libpcre2-dev,
 libssl-dev,
 uuid-dev,
 zlib1g-dev,
 libcap-dev [linux-any],
 libjail-dev [kfreebsd-any],
 binutils,
 gcc,
 python3,
 uwsgi-core,
 libc6-dev | libc-dev,
 libbsd-dev [kfreebsd-any],
 ${misc:Depends},
 ${perl:Depends},
Recommends:
 git,
 cppcheck,
Enhances:
 debhelper,
Provides:
 dh-sequence-uwsgi,
Description: fast, self-healing application container server (headers)
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 .
 This package provides development headers and tools needed to develop
 external uWSGI plugins, either contained in this package or pulled in
 through depending on development packages of dependent projects.

Package: uwsgi-emperor
Architecture: all
Depends:
 uwsgi-core,
 ${misc:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Description: fast, self-healing application container server (emperor scripts)
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It uses the uwsgi protocol for all the networking/interprocess communications.
 .
 uWSGI can be run in preforking, threaded, asynchronous/evented modes and
 supports various forms of green threads/coroutines (such as uGreen, Greenlet,
 Fiber). uWSGI provides several methods of configuration: via command line,
 via environment variables, via XML, INI, YAML configuration files, via LDAP
 and more.
 .
 On top of all this, it is designed to be fully modular. This means that
 different plugins can be used in order to add compatibility with tons of
 different technology on top of the same core.
 .
 This package depends on uWSGI core binary and installs:
  * init.d script for running uWSGI Emperor daemon with options defined in
    custom user-created configuration files
  * basic configuration for running uWSGI Emperor daemon

Package: uwsgi-extra
Architecture: all
Depends:
 ${misc:Depends},
Description: fast, self-healing application container server (extra files)
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 .
 This package provides extra files from uWSGI distribution.

Package: uwsgi-plugin-alarm-curl
Architecture: any
Depends:
 uwsgi-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: cURL alarm plugin for uWSGI
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It is designed to be fully modular. This means that different plugins can be
 used in order to add compatibility with tons of different technology on top of
 the same core.
 .
 This package provides alarm_curl plugin for uWSGI.

Package: uwsgi-plugin-alarm-xmpp
Architecture: any
Depends:
 uwsgi-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: XMPP alarm plugin for uWSGI
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It is designed to be fully modular. This means that different plugins can be
 used in order to add compatibility with tons of different technology on top of
 the same core.
 .
 This package provides alarm_xmpp plugin for uWSGI.

Package: uwsgi-plugin-curl-cron
Architecture: any
Depends:
 uwsgi-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: cron cURL plugin for uWSGI
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It is designed to be fully modular. This means that different plugins can be
 used in order to add compatibility with tons of different technology on top of
 the same core.
 .
 This package provides curl_cron plugin for uWSGI.

Package: uwsgi-plugin-emperor-pg
Architecture: any
Depends:
 uwsgi-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Emperor PostgreSQL plugin for uWSGI
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It is designed to be fully modular. This means that different plugins can be
 used in order to add compatibility with tons of different technology on top of
 the same core.
 .
 This package provides emperor_pg plugin for uWSGI.

Package: uwsgi-plugin-geoip
Architecture: any
Depends:
 uwsgi-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: GeoIP plugin for uWSGI
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It is designed to be fully modular. This means that different plugins can be
 used in order to add compatibility with tons of different technology on top of
 the same core.
 .
 This package provides geoip plugin for uWSGI.

Package: uwsgi-plugin-graylog2
Architecture: any
Depends:
 uwsgi-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: graylog2 plugin for uWSGI
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It is designed to be fully modular. This means that different plugins can be
 used in order to add compatibility with tons of different technology on top of
 the same core.
 .
 This package provides graylog2 plugin for uWSGI.

Package: uwsgi-plugin-ldap
Architecture: any
Depends:
 uwsgi-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: LDAP plugin for uWSGI
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It is designed to be fully modular. This means that different plugins can be
 used in order to add compatibility with tons of different technology on top of
 the same core.
 .
 This package provides ldap plugin for uWSGI.

Package: uwsgi-plugin-router-access
Architecture: any
Depends:
 uwsgi-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Access router plugin for uWSGI
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It is designed to be fully modular. This means that different plugins can be
 used in order to add compatibility with tons of different technology on top of
 the same core.
 .
 This package provides router_access plugin for uWSGI.

Package: uwsgi-plugin-sqlite3
Architecture: any
Depends:
 uwsgi-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 sqlite3,
Description: SQLite 3 configurations plugin for uWSGI
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It is designed to be fully modular. This means that different plugins can be
 used in order to add compatibility with tons of different technology on top of
 the same core.
 .
 This package provides sqlite3 plugin for uWSGI, to load configurations
 from SQLite 3 database.

Package: uwsgi-plugin-xslt
Architecture: any
Depends:
 uwsgi-core (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: XSLT request plugin for uWSGI
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 It is designed to be fully modular. This means that different plugins can be
 used in order to add compatibility with tons of different technology on top of
 the same core.
 .
 This package provides xslt plugin for uWSGI.

Package: uwsgi-src
Architecture: all
Multi-Arch: foreign
Section: libdevel
Depends:
 ${misc:Depends},
 ${perl:Depends},
Suggests:
 uwsgi-dev,
Description: sources for uWSGI plugins
 uWSGI presents a complete stack for networked/clustered web applications,
 implementing message/object passing, caching, RPC and process management.
 .
 This package contains the sources for uWSGI plugins.
